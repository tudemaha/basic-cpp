#include <iostream>
#include <string>

using namespace std;

    /*
        ternary operator adalah simplifikasi dari IF
        kondisi ? hasil_benar : hasil_salah;
    */

int main() {
    int a, b;
    string output, benar = "ini benar", salah = "ini salah";

    a = 8;
    b = 6;
    
    // contoh sederhana
    a == b ? cout << "a sama dengan b" : cout << "a tidak sama dengan b";

    // contoh lagi
    output = (a < b) ? "a kurang dari b" : "a lebih dari b";
    cout << endl << output << endl;

    // contoh lagi
    output = (a < b) ? benar : salah;
    cout << output;




    return 0;
}