#include <iostream>

using namespace std;

/*
    Comma operator
    (expression1, expression2, expression3, ...); // dikurung dalam tanda ()
*/

void cetak (int bilangan) {
    cout << bilangan << endl;
}

int main() {
    int a;
    int b;
    int c;

    // bisa seperti ini
    //a = (b = 1, c = 4);
    //a = b + c;

    // bisa seperti ini juga
    //a = (b = 1, c = 4, a = b + c);
    
    // bisa begini
    //a = (b = 1, c = 4, cout << b << endl, cout << c << endl, a = b + c);

    // bisa seperti ini
    a = (b = 1, c = 4, cetak(b), cetak(c), a = b + c);

    cetak(a);

    // akan menghasilkan hal yang sama
    

    return 0;
}