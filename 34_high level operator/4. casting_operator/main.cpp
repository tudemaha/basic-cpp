#include <iostream>

using namespace std;

int main() {
    int a = 5;
    float b = 3.86f;
    char c = 'e';

    // menggunakan inplisit casting
    cout << a + b << endl;
    cout << a + c << endl;

    // eksplisit casting
    cout << (float)a + b << endl;
    cout << a + (int)b << endl;
    cout << (char)a + c << endl;
    cout << (char)(a + c) << endl; // harus casting hasil operasi agar menghasilkan char

    return 0;
}