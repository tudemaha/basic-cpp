#include <iostream>
#include <bitset>
#include <string>

using namespace std;

/*
    Bitwise operators:
    & [AND (Bitwise AND)]
    | [OR (Bitwise inclusive OR)]
    ^ [XOR (Bitwise exlusive OR)]
    ~ [NOT]
    << [SHL (Shift bits left)]
    >> [SHR (Shift bits right)]
*/

unsigned short a = 5, b = 7, c;

void cetakBiner(unsigned short bit) {
    cout << "a :" << bitset<8>(a) << endl;
    cout << "b: " << bitset<8>(b) << endl;
    cout << "c: " << bitset<8>(bit) << endl;
    cout << "c: " << c << endl;
}

int main() {
    

    cout << "AND" << endl;
    c = a & b;
    cetakBiner(c);

    cout << "OR" << endl;
    c = a | b;
    cetakBiner(c);

    cout << "XOR" << endl;
    c = a ^ b;
    cetakBiner(c);

    cout << "NOT" << endl;
    c = ~a;
    cetakBiner(c);

    cout << "SHL" << endl;
    c = a << 3; // menggeser n ke kiri (nilai dipindahkan ke address lain)
    cetakBiner(c);

    cout << "SHR" << endl;
    c = a >> 2; // menggeser n ke kanan (nilai dipindahkan ke address lain)
    cetakBiner(c);
    return 0;
}