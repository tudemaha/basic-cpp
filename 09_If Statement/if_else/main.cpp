#include <iostream>

using namespace std;

int main() {
    int a;

    cout << "Masukkan angka = ";
    cin >> a;

    // if statement
    if(a == 3) {
        cout << "Nilai yang dimasukkan adalah 3" << endl;
    } else if (a == 5) {
        cout << "Nilai yang dimasukkan adalah 5" << endl;
    } else if (a == 1) {
        cout << "Nilai yang dimasukkan adalah 1" << endl;
    } else {
        cout << "Nilai yang dimasukkan tidak terdaftar" << endl;
    }

    cout << "Selesai" << endl;

    cin.get();
    return 0;
}
