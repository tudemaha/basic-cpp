#include <iostream>

using namespace std;

int main() {
    string kalimat1 = "Aku suka makan sayur brokoli";
    string kalimat2 = "Dia makan sate di luar rumah";

    cout << "1: " << kalimat1 << endl;
    cout << "2: " << kalimat2 << endl;

    // subtring, mengambil string di tengah-tengah
    // substring(index, panjang)
    cout << kalimat1.substr(9, 5) << " ";
    cout << kalimat2. substr(23, 5) << endl;

    // mencari posisi dari substring
    cout << "posisi makan: " << kalimat1.find("makan") << endl;
    cout << "posisi rumah: " << kalimat2.find("rumah") << endl;

    int a = kalimat1.find("ka");
    cout << a << endl;
    cout << kalimat1.find("ka", a + 1) << endl; // find("kata", dimulai_dari) agar bisa mencari kata selanjutnya


    // mencari posisi dari belakang -> rfind (reverse find)
    cout << kalimat2.rfind("lu") << endl << endl;

    // program mencari posisi a di mana saja
    cout << "Posisi 'a' di kalimat2: ";
    int tempat = 0;
    while(true) {
        tempat = kalimat2.find("a", tempat);

        if(tempat < 0) break;

        cout << tempat << " ";
        
        tempat++;
        
    }

    return 0;
}