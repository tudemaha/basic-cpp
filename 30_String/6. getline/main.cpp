#include <iostream>
#include <array>

using namespace std;

int main() {
    string kalimatInput;

    cout << "Masukkan kalimat: ";
    getline(cin, kalimatInput); //berasal dari library iostream

    cout << "Kalimat yang dimasukkan: " << kalimatInput << endl;

    // menghitung jumlah kata
    int jumlah = 0, posisi = 0;
    while(true) {
        posisi = kalimatInput.find(" ", posisi + 1);
        jumlah++;
        
        if(posisi < 0) break;
    }

    cout << "Jumlah kata: " << jumlah;

    return 0;
}