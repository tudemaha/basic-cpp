#include <iostream>
#include <string>
#include <string.h> // library bahasa C

using namespace std;

int main() {
    // pada bahasa C (akan lebih sulit karena membandingkan tiap karakter)
    char kata[] = "ucup", pembanding[] = "roy";
    if(strcmp(kata, pembanding) == 0) cout << "sama!!" << endl;
    else cout << "tidak sama!" << endl;

    // pada C++
    string word = "ucup", word2 ="ucup";
    if(word == word2) cout << "Sama!!" << endl; //tinggal bandingkan dengan ==
    else cout << "tidak sama!" << endl;

    // program perbandingan string sederhana
    string tebakan;
    while(true) {
        cout << "Masukkan tebakan nama: ";
        cin >> tebakan;
        if(word == tebakan) {
            cout << "Benarrr!!" << endl;
            break;
        }
        cout << "Tebakan salah!" << endl;
    }

    cout << "Program selesai.";

    return 0;
}