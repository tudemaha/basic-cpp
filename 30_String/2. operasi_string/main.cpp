#include <iostream>
#include <string>

using namespace std;

int main() {
    string kata = "cat";

    // menampilkan data string
    cout << kata << endl;

    // mengambil karakter berdasarkan index
    cout << "index ke 0: " << kata[0] << endl;
    cout << "index ke 1: " << kata[1] << endl;
    cout << "index ke 2: " << kata[2] << endl;
    cout << "index lainnya: " << kata[3] << endl;   // tidak akan menampilka apa-apa

    // mengubah karakter pada string
    kata[1] = 'e';
    cout << kata << endl;

    // menyambug (concatenation)
    // cara 1
    string kata2 = kata + "ar"; // string kata2(kata + "ar");
    cout << kata2 << endl;

    // cara 2
    string kata3 = " membahana";
    kata2.append(kata3);
    cout << kata2 << endl;

    // cara 3 (paling mudah)
    string kata4 = "yuhuu!!!";
    kata2 += " " + kata4; // seperti operasi artimatika
    cout << kata2 << endl;

    return 0;
}