#include <iostream>
#include <string>

using namespace std;

int main() {
    // pembuatan string pertama
    string data1 = "mobil";

    // pembuatan string kedua
    string data2("mobil");

    string kata;

    cout << data1 << endl;
    cout << data2 << endl;

    cout << "Masukkan kata: ";
    cin >> kata;
    cout << "Kata yang dimasukkan adalah " << kata << endl;
    return 0;
}