#include <iostream>
#include <string>

using namespace std;

int main() {
    string kalimat1 = "Halo kalian di sana!!";
    string kalimat2 = "Haii kalian wahai saudara-saudara!";

    cout << "1: " << kalimat1 << endl;
    cout << "2: " << kalimat2 << endl << endl;

    // swap string
    kalimat1.swap(kalimat2);
    cout << "Swap string" << endl;
    cout << "1: " << kalimat1 << endl;
    cout << "2: " << kalimat2 << endl << endl;

    // replace (mengganti) string
    kalimat2.replace(15, 4, "sini");
    kalimat1.replace(kalimat1.find("kalian"), 6, "kamu");

    cout << "Replace String" << endl;
    cout << "1: " << kalimat1 << endl;
    cout << "2: " << kalimat2 << endl << endl;

    // insert string
    kalimat2.insert(kalimat2.find("sini") + 4, " dan sana");
    cout << "Insert string" << endl;
    cout << "1: " << kalimat1 << endl;
    cout << "2: " << kalimat2 << endl;

    return 0;
}