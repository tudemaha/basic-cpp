#include <iostream>
#include <string>
#include <fstream>

using namespace std;

    /*
        ifstream
        ios::in
        ios::ate -> mulai dari akhir file
        ios::binary -> membaca file biner
    */

int main() {
    ifstream myFile;
    string output, buffer;
    int no;
    string nama;

    myFile.open("data.txt", ios::in);

    // ini akan mengambil data per kata
    // myFile >> data;
    // myFile >> data;

    

    // mengambil data per baris
    // getline(myFile, buffer);
    // output.append(buffer);
    // getline(myFile, buffer);
    // output.append("\n" + buffer);

    // mengambil sampai kata 'data'
    while(true) {
        getline(myFile, buffer);
        output.append("\n" + buffer);
        if(buffer == "data") break;
    }

    cout << output << endl;

    // mengambil data no dan nama
    getline(myFile, buffer);
    cout << buffer << endl;

    int jumlahData = 0;
    while(!myFile.eof()) {
        myFile >> no;
        myFile >> nama;

        jumlahData++;
        cout << no << "\t" << nama << endl;

    }

    cout << "Jumlah data: " << jumlahData << endl;
    

    return 0;
}