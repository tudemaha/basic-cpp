#include <iostream>
#include <string>
#include <fstream>

using namespace std;

struct mahasiswa {
    int nim;
    string nama;
    string jurusan;
};

int main() {
    fstream myFile;

    myFile.open("data.bin", ios::trunc | ios::out | ios::in | ios::binary);
    mahasiswa mahasiswa1, mahasiswa2, mahasiswa3;

    mahasiswa1.nim = 123;
    mahasiswa1.nama = "Anto";
    mahasiswa1.jurusan = "Informatika";

    mahasiswa2.nim = 176;
    mahasiswa2.nama = "Made";
    mahasiswa2.jurusan = "Mesin";

    mahasiswa3.nim = 874;
    mahasiswa3.nama = "Susanti";
    mahasiswa3.jurusan = "Sipil";

    // tuliskan ke file bin
    myFile.write(reinterpret_cast<char*>(&mahasiswa1), sizeof(mahasiswa));  //bisa pakai sizeof(mahasiswa) atau sizeof(mahasiswa1)
    myFile.write(reinterpret_cast<char*>(&mahasiswa2), sizeof(mahasiswa));
    myFile.write(reinterpret_cast<char*>(&mahasiswa3), sizeof(mahasiswa));


    myFile.close();

    

    return 0;
}