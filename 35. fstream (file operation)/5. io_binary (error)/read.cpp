#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct mahasiswa {
    int nim;
    string nama;
    string jurusan;
};

int main() {
    fstream myFile;
    myFile.open("data.bin", ios::in | ios::binary);
    mahasiswa mahasiswa_baca;

    // melihat ukuran struct mahasiswa
    // cout << sizeof(mahasiswa) << endl;
    // 0-----72-----|-----72-----|-----72------|

    // seek position (kelipatan dari ukuran struct)
    myFile.seekp(0 * sizeof(mahasiswa), ios_base::end);    // --> menyebabkan error

    myFile.read(reinterpret_cast<char*>(&mahasiswa_baca), sizeof(mahasiswa));

    cout << mahasiswa_baca.nim << endl;
    cout << mahasiswa_baca.nama << endl;
    cout << mahasiswa_baca.jurusan << endl;

    myFile.close();


    return 0;
}