#include <iostream>
#include <fstream>
#include <array>

using namespace std;

int main() {
    fstream myFile;
    int number = 896768;

    myFile.open("data.bin", ios::out | ios::binary);

    // untuk menulis dalam bentuk integer
    // myFile << number;

    // menulis dalam bentuk biner
    myFile.write(reinterpret_cast<char*>(&number), sizeof(number));

    myFile.close();


    return 0;
}