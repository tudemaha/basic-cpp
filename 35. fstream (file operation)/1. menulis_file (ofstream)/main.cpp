#include <iostream>
#include <fstream> // ofstream, ifstream, fstream

using namespace std;

int main() {
    /*
        ios::out -> default, operasi output
        ios::app -> menuliskan pada akhri haris
        ios::trunc -> default, overwrite file
    */

   ofstream myFile;

   myFile.open("data1.txt", ios::out);
   myFile << "menuliskan baris baru pada data1\n";
   myFile.close();

   myFile.open("data2.txt", ios::trunc);
   myFile << "menuliskan baris baru pada data2\n";
   myFile.close();

   myFile.open("data3.txt", ios::app);
   myFile << "menuliskan baris baru pada data3\n";
   myFile.close();
   
   // ios::out memiliki perlakukan yang sama dengan ios::trunc

    return 0;
}