#include <iostream>
// # => preprocessing directive, tidak ada hubungannya dengan compiler/memory

// macro
#define PI 3.1415 // => ini hanya mengganti nilai (tidak memakai memori/tidak memiliki address)

using namespace std;

int main() {
    double pi = 3.1415;

    cout << "Nilai pi = " << PI << endl;
    cout << "Nilai pi double = " << pi << endl;
    cout << PI * 3 << endl; // bisa dilakukan operasi


    return 0;
}