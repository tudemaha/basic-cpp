#include <iostream>
#include "bersama.h"
#include "foo.h"
#include "bar.h"

// // ifdef => mengecek sesuatu sudah didefine atau belum
// #ifdef FOO
//     #define TEST_FOO "foo ada"
// #else
//     #define TEST_FOO "foo tidak ada"
// #endif

// // ifndef => mengecek jika sesuatu tidak ada
// #ifndef FOO
//     #define FOO "foo baru"
// #endif

using namespace std;

int main() {
    // cout << FOO << endl;
    Mahasiswa mahasiswa;
    mahasiswa.NIM = 10;
    cout << mahasiswa.NIM << endl;

    return 0;
}