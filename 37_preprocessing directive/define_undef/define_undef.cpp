// preprocessing directive
#include <iostream>

// macro merubah nilai dengan nama
#define PI 3.141592 // => tidak menggunakan memori
#define BAHASA "Indonesia"

// macro untuk fungsi
#define KUADRAT(X) (X * X)
#define MAX(A, B) ((A > B) ? A : B)



// end of preprocessing directive

using namespace std;

int main() {
    
    cout << "Nilai PI: " << PI << endl;
    cout << "Bahasa: " << BAHASA << endl;
    cout << "Kuadrat: " << KUADRAT(6) << endl;
    cout << "Max: " << MAX(5, 10) << endl;

    // bisa digunakan di dalam fungsi
    #undef BAHASA
    #define BAHASA "Inggris"
    cout << BAHASA << endl;

    return 0;
}