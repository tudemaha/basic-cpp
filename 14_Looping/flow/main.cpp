#include <iostream>

using namespace std;

int main() {
    for(int i = 1; i <= 10; i++) {
        if(i == 5) {
            break; //Break berfungsi untuk menhentikan code (menghentikan loop)
        }
        cout << "Print ke-" << i << endl;
    }


    return 0;
}
