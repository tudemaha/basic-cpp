#include <iostream>

using namespace std;

int main() {
    int i = 5;

    do {
        cout << "Hore ke-" << i << endl;
        i++;
    }
    while(i <= 5);

    return 0;
}
