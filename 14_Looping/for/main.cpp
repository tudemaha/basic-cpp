#include <iostream>

using namespace std;

int main() {
    //for 1
    for(int i = 1; i <= 5; i++) {
        cout << "Hore " << i << endl;
    }
    cout << "\n";

    //for 2
    for(int i = 10; i >= 1; i--) {
        cout << "Aku ngeprint ke-" << i << endl;
    }
    cout << "\n";

    //for 3
    int harga = 0;
    for(int i = 1; i <= 10; i++, harga += i) {
        cout << i << "||" << harga << endl;
    }
    cout << "\n";

    //for 4
    for(int i = 1; i <= 10; i += 2) {
        cout << i << endl;
    }
    return 0;
}
