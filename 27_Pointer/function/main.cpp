#include <iostream>

using namespace std;

//Prototype
void fungsi(int *);
void kuadrat(int *);

int main() {
    int a = 5;

    cout << "address a: " << &a << endl;
    cout << "nilai a: " << a << endl;

    fungsi(&a);
    kuadrat(&a);

    cout << "address a: " << &a << endl;
    cout << "nilai a: " << a << endl;


    return 0;
}

void fungsi(int *b) {
    cout << "address b: " << b << endl;
    cout << "nilai b: " << *b << endl; // dereferencing
}

void kuadrat(int *value) {
    *value = (*value) * (*value);
}
