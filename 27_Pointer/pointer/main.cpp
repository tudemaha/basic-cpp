#include <iostream>

using namespace std;

int main() {
    int a = 10;

    // pointer
    int *aPtr = nullptr; // variabel untuk pointer diawali dengan * (nullptr adalah alamat kosong)
    aPtr = &a; // &a untuk mengambil alamat dari a

    // int a mempunyai nilai dan address (alamat)
    cout << "Nilai dari a: " << a << endl;
    cout << "Alamat dari a: " << aPtr << endl;

    // Alamat selalu berubah-ubah tetapi nilainya tetap sama

    a = 5;
    cout << "Mengambil nilai dari pointer aPtr: " << *aPtr << endl;
    // * dapat diartikan sebagai tanda untuk on atau off penampilan address


    return 0;
}
