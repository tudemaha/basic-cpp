#include <iostream>

using namespace std;

int main() {
    int a = 10;
    cout << "Nilai awal a: " << a << endl;

    a += 2;
    cout << "Nilai a setelah ditambah 2: " << a << endl;

    a -= 2;
    cout << "Nilai a setelah dikurangi 2: " << a << endl;

    a /= 2;
    cout << "Nilai a setelah dibagi 2: " << a << endl;

    a *= 2;
    cout << "Nilai a setelah dikali 2: " << a << endl;

    a %= 2;
    cout << "Nilai a setelah dimodulus 2: " << a << endl;



    return 0;
}
