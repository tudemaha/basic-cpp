#include <iostream>
#include <fstream>
#include <string>
#include <limits>

using namespace std;

struct Mahasiswa {
    int pk;
    string nim;
    string nama;
    string prodi;
};

int getInput();
void dbCheck(fstream &db);

void writeData(fstream &db, int posisi, Mahasiswa data_mahasiswa) {
    // db.seekp(posisi, mode)
    db.seekp((posisi - 1) * sizeof(Mahasiswa), ios::beg);
    db.write(reinterpret_cast<char*>(&data_mahasiswa), sizeof(Mahasiswa));
}

// mendapatkan size data
int getDataSize(fstream &db) {
    int start, end;
    db.seekg(0, ios::beg);
    start = db.tellg();
    db.seekg(0, ios::end);
    end = db.tellg();

    return (end - start) / sizeof(Mahasiswa);
}

Mahasiswa readData(fstream &db, int posisi) {
    Mahasiswa readMahasiswa;
    db.seekg((posisi - 1) * sizeof(Mahasiswa), ios::beg);
    db.read(reinterpret_cast<char*>(&readMahasiswa), sizeof(Mahasiswa));

    return readMahasiswa;
}

void inputData(fstream &db) {
    // untuk menghilangkan enter dari cin (cara sederhana)
    // while(getchar() != '\n');

    Mahasiswa input_data, last_data;

    int size = getDataSize(db);
    cout << "ukuran data: " << size << endl;

    if(size == 0) {
        input_data.pk = 0;
    } else {
        last_data = readData(db, size);
        cout << "pk = " << last_data.pk << endl;
        input_data.pk = last_data.pk + 1;
    }

    cout << "Nama: ";
    getline(cin, input_data.nama);
    cout << "NIM: ";
    getline(cin, input_data.nim);
    cout << "Prodi: ";
    getline(cin, input_data.prodi);

    writeData(db, size + 1, input_data);
}


int main() {
    // buat objek fstream
    fstream db_mahasiswa;

    // cek ketersediaan database
    dbCheck(db_mahasiswa);
    
    int pilihan = getInput();
    char is_continue;

    enum aksi {CREATE = 1, READ, UPDATE, DELETE, FINISH};

    while(pilihan != FINISH) {
        switch(pilihan) {
            case CREATE:
                cout << "Tambah data mahasiswa" << endl;
                inputData(db_mahasiswa);
                break;
            case READ:
                cout << "Tampilkan data mahasiswa" << endl;
                break;
            case UPDATE:
                cout << "Ubah data mahasiswa" << endl;
                break;
            case DELETE:
                cout << "Hapus data mahasiswa" << endl;
                break;
            default:
                cout << "Pilihan tidak valid!" << endl;
                break;
        }

        label_continue:
        cout << "Lanjutkan? (y/n): ";
        cin >> is_continue;
        if(is_continue == 'y' | is_continue == 'Y') {
            pilihan = getInput();
        } else if(is_continue == 'n' | is_continue == 'N') {
            break;
        } else {
            goto label_continue;
        }
    }

    cout << "Program berakhir" << endl;

    
    return 0;
}

// menangkap input pilihan user
int getInput() {
    // system("cls");

    int input;

    cout << "PROGRAM CRUD DATA MAHASISWA" << endl;
    cout << "===========================" << endl;
    cout << "1. Tambah data mahasiswa" << endl;
    cout << "2. Lihat data mahasiswa" << endl;
    cout << "3. Ubah data mahasiswa" << endl;
    cout << "4. Hapus data mahasiswa" << endl;
    cout << "5. Selesai" << endl;
    cout << "===========================" << endl;
    cout << "Masukkan pilihan [1-5]: ";
    cin >> input;
    // untuk menghilangkan enter dari cin (dengan library limits)
    cin.ignore(numeric_limits<streamsize>::max(), '\n'); 

    return input;
}

// cek ketersediaan database
void dbCheck(fstream &db) {
    db.open("database.bin", ios::out | ios::in | ios::binary);

    if(db.is_open()) {
        cout << "Database ditemukan." << endl;
    } else {
        cout << "Database tidak ditemukan, database baru dibuatkan." << endl;
        db.close();
        db.open("database.bin", ios::trunc | ios::out | ios::in | ios::binary);
    }
}