#include <iostream>

using namespace std;

int main() {
    int n;

    // Deklarasi variabel untuk rumus fibonaci (fn = fn1 + fn2)
    int fn;
    int fn1 = 0;
    int fn2 = 1;

    cout << "Masukkan n deret Fibonaci pertama yang ingin diketahui: ";
    cin >> n;
    cout << "\n" << n << " deret Fibonaci pertama:" << endl;


    cout << fn2;
    for(int i = 1; i < n; i++) {
        fn = fn1 + fn2;
        fn1 = fn2;
        fn2 = fn;
        cout << " " << fn;
    }

    return 0;
}
