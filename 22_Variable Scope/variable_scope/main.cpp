#include <iostream>

using namespace std;

int x = 10; // Global variabel

int ambilGlobal() {
    return x;
}

int main() {
    cout << "1. Global variabel x: " << x << endl;

    int x = 1; // Local main variable
    cout << "2. Local main variabel x: " <<x << endl;
    cout << "3. Global variabel x: " << ambilGlobal() << endl;

    {
        cout << "4. Variabel local main: " << x << endl;
        int x = 7;
        cout << "5. Variabel block x: " << x << endl;
    }

        cout << "6. Variabel local main: " << x << endl;
        cout << "7. Global variabel x: " << ::x << endl; //Untuk mengambil variabel terluar


    return 0;
}
