// Headers
#include <iostream>

// Ini adalah entry point
int main()

// Ini body
{
	/*
	std = standard library berasal dari iostream
	cout = console out (untuk menampilkan text)
	<< untuk memasukkan input ke cout
	endl = akhir dari bari
	*/
	std::cout << "Hello World!" << std::endl;
	
	// return 0 berhubungan  dengan int
	return 0;
}
