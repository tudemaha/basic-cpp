#include <iostream>

using namespace std;

int main() {
    int a = 4;
    int b = 4;

    // Increment dan decrement
    // Post-increment
    cout << a << endl;
    cout << a-- << endl; //Diprint dulu, baru ditambahkan
    cout << a << endl << endl;

    //Pre-increment
    cout << b << endl;
    cout << --b << endl; // Ditambah dulu baru diprint
    cout << b << endl;

}
