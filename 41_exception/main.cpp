#include <iostream>
#include <array>
#include <exception>

using namespace std;

/*
1. syntax error => kesalahan saat salah memasukkan grammar bahasa pemrograman
    (kurang titik koma, kurung, kurang huruf, dll)
2. linking error
    (kesalahan saat membuat prototype tapi tidak ada fungsi)
3. non-error
    (kesalahan logika program, misalnya membuat luas menjadi negatif)
4. runtime error => error yang terjadi saat program berjalan
    misalnya pembagian dengan 0 pada perulangan
*/  

int pembagian(int &num, int &denum) {
    return num / denum;
}


int main() {
    // // contoh runtime error
    // int a;
    // int b;
    // int c;
    // char is_lanjut;

    // while(true) {
    //     cout << "num: ";
    //     cin >> a;
    //     cout << "denum: ";
    //     cin >> b;
        
    //     c = pembagian(a, b);
    //     cout << c << endl;

    //     cout << "lanjutkan? (y/n): ";
    //     cin >> is_lanjut;
    //     if(is_lanjut == 'n' || is_lanjut == 'N') break;
    // }

    array<int, 5> a = {0, 1, 2, 3, 4};
    // cout << a.at(5) << endl;    // class at() membuat runtime error

    // menangani runtime error
    try {
        cout << a.at(5) << endl;
    } catch(exception &e) {
        cout << e.what() << endl;
    }

    cout << "Akhir dari program" << endl;

    return 0;
}