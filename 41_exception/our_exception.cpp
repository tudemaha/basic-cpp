#include <iostream>
#include <exception>

/*
cara 1: menggunakan cara manual
cara 2: menggunakan library exception
*/

using namespace std;

int pembagian(int &num, int &denum) {
    if(denum == 0) {
        // throw "Error: pembagi nol";
        throw overflow_error("Error: pembagi nol");
    }
    return num / denum;
}


int main() {
    int a, b, c;

    while(true) {
        cout << "num: ";
        cin >> a;
        cout << "denum: ";
        cin >> b;

        try {
            c = pembagian(a, b);
            cout << c << endl;
        // } catch(const char *e) {
        }catch(exception &e){ // cara 2
            // cout << e << endl;
            cout << e.what() << endl;
        }
    }
    
    cout << "akhir dari program" << endl;

    return 0;
}