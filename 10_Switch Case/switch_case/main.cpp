#include <iostream>

using namespace std;

int main() {
    int a;

    cout << "Masukkan nilai = ";
    cin >> a;

    switch(a) {
        case 5:
            cout << "Ya itu adalah 5" << endl;
            break;
        case 6:
        case 4:
            cout << "Ini 6 atau 4" << endl;
            break;
        default:
            cout << "Angka tidak terdaftar" << endl;
    }


    cout << "Akhir program" << endl;
    return 0;
}
