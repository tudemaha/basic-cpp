#include <iostream>
#include "otong.h"

// namespace => untuk membuat sekup pada fungsi

void fungsi() {
    std::cout << "Ini adalah fungsi biasa" << std::endl;
}

int main() {
    fungsi();
    std::cout << otong::a << std::endl;
    otong::fungsi();
    otong::cout(1000);

    return 0;
}