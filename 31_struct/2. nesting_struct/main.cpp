#include <iostream>
#include <string>

using namespace std;

struct aktor {
    string nama;
    int tahun_lahir;
};

struct film {
    string judul;
    string genre;
    int tahun;
    aktor pemeran1;
    aktor pemeran2;
};

int main() {

    aktor aktor1, aktor2;
    film film_bagus;

    // buat data aktor
    aktor1.nama = "Otong Surotong";
    aktor1.tahun_lahir = 1990;

    aktor2.nama = "Anji Jumanji";
    aktor2.tahun_lahir = 1992;

    // buat data film
    film_bagus.judul = "Avatar Shippuden";
    film_bagus.genre = "Random";
    film_bagus.tahun = 2008;
    film_bagus.pemeran1 = aktor1;
    film_bagus.pemeran2 = aktor2;

    // cetak data film
    cout << film_bagus.judul << endl;
    cout << film_bagus.genre << endl;
    cout << film_bagus.tahun << endl;
    cout << film_bagus.pemeran1.nama << endl;
    cout << film_bagus.pemeran1.tahun_lahir << endl;
    cout << film_bagus.pemeran2.nama << endl;
    cout << film_bagus.pemeran1.tahun_lahir << endl;

    return 0;
}