#include <stdio.h>
#include <string.h>

struct aktor {
    char nama[50];
    int tahun_lahir;
};

struct film {
    char judul[50];
    char genre[20];
    int tahun;
    struct aktor pemeran1;
    struct aktor pemeran2;
};

int main() {
    // deklarasi
    struct aktor aktor1, aktor2;
    struct film filmBagus;

    // data aktor
    strcpy(aktor1.nama, "Otong Surotong");
    aktor1.tahun_lahir = 1990;

    strcpy(aktor2.nama, "Anji Jumanji");
    aktor2.tahun_lahir = 1992;

    // data film
    strcpy(filmBagus.judul, "Otong Mencari Cinta");
    strcpy(filmBagus.genre, "Cinta");
    filmBagus.tahun = 2019;
    filmBagus.pemeran1 = aktor1;
    filmBagus.pemeran2 = aktor2;

    // cetak data
    printf("%s\n", filmBagus.judul);
    printf("%d\n", filmBagus.tahun);
    printf("%s\n", filmBagus.pemeran1.nama);
    printf("%s\n", filmBagus.pemeran2.nama);

    return 0;
}