#include <stdio.h>
#include <string.h>

struct buah {
    int harga;
    float berat;
    char warna[50];
    char rasa[50];
};

int main() {
    struct buah jeruk;

    jeruk.berat = 150.0F;
    jeruk.harga = 20000;
    strcpy(jeruk.rasa, "asam");
    strcpy(jeruk.warna, "orange");

    printf("JERUK\n\n");
    printf("%.2f\n", jeruk.berat);
    printf("%d\n", jeruk.harga);
    printf("%s\n", jeruk.rasa);
    printf("%s\n", jeruk.warna);

    return 0;
}