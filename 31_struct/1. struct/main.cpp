#include <iostream>
#include <string>

using namespace std;

struct buah {
    string warna;
    float berat;
    int harga;
    string rasa;
};

int main() {
    /*
    Struct : data yang dibentuk oleh beberapa data
    jeruk = buah;
    jeruk punya:
    jeruk.warna
    jeruk.rasa
    jeruk.harga
    jeruk.berat
    */

   buah apel;
   buah jeruk;

   apel.berat = 150.4F;
   apel.harga = 20000;
   apel.rasa = "manis";
   apel.warna = "merah";
   
   jeruk.berat = 100.F;
   jeruk.harga = 15000;
   jeruk.rasa = "asam";
   jeruk.warna = "orange";

   cout << "APEL" << endl;
   cout << apel.berat << endl;
   cout << apel.harga << endl;
   cout << apel.rasa << endl;
   cout << apel.warna << endl << endl;

   cout << "JERUK" << endl;
   cout << jeruk.berat << endl;
   cout << jeruk.harga << endl;
   cout << jeruk.rasa << endl;
   cout << jeruk.warna << endl << endl;

    return 0;
}