#include <iostream>

using namespace std;

int main() {
    int a = 3;
    int b = 2;

    bool hasil;

    // Operator logika: not, and, or

    // not
    hasil = !(a == 3);

    // and (&&) => kedua nilai harus benar untuk menghasilkan nilai true
    hasil = (a == 3) && (b == 2);

    // or (||) => salah satu nilai harus benar untuk menghasilkan nilai true
    hasil = (a == 3) || (b == 3);
    cout << hasil << endl;

    cin.get();
    return 0;
}
