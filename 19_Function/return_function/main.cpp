#include <iostream>

using namespace std;

int kuadrat(int x) {
    int hasil = x * x;

    return hasil;
}

int kali(int a, int b) {
    int hasil = a * b;

    return hasil;
}

int main() {
    int a, x, y;

    cout << "Masukkan bilangan untuk dikuadratkan: ";
    cin >> a;
    cout << kuadrat(a) << endl;

    cout << "Masukan bilangan pertama untuk dikalikan: ";
    cin >> x;
    cout << "Masukkan bilangan kedua untuk dikalikan: ";
    cin >> y;
    cout << kali(x, y);
}
