#include <iostream>

using namespace std;

void perkenalan(string nama) {
    cout << "Halo! Perkenalkan nama saya " << nama << endl;
}

int main() {
    string nama;

    cout << "Masukkan nama untuk diperkenalkan: ";
    cin >> nama;
    perkenalan(nama);

    return 0;
}
