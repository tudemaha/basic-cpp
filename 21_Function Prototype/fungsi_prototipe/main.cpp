#include <iostream>

using namespace std;

// Prototype fungsi (Deklarasi fungsi yang diletakkan di akhir body)
// Bisa digunakan untuk fungsi dengan atau tanpa kembalian
double luas(double sisi);


int main() {
    double sisi;

    cout << "APLIKASI PENGHITUNG LUAS PERSEGI \n";
    cout << "Masukkan panjang sisi: ";
    cin >> sisi;


    cout << "Luas persegi: " << luas(sisi) << endl;

    return 0;
}

double luas(double sisi) {
    double luas = sisi * sisi;

    return luas;
}
