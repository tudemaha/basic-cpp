#include <iostream>

using namespace std;

int fibonacci(int n);

int main() {
    int angka = 5;
    angka--;


    cout << fibonacci(angka) << endl;
    return 0;
}

// Tidak efektif, menghabiskan banyak memori
int fibonacci(int n) {
    if(n == 0 || n == 1) {
        return n;
    }
    return fibonacci(n-1) + fibonacci(n-2);
}
