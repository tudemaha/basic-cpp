#include <iostream>

using namespace std;

// Prototype
int factorial(int n);

int main() {
    int n;
    cout << "Menghitung faktorial dari: ";
    cin >> n;

    int hasil = factorial(n);
    cout << "\nNilai faktorialnya adalah " << hasil << endl;

}

int factorial(int n) {
    if(n == 1) {
        cout << n;
        return n;
    }

    cout << n << "*";
    return n * factorial(n-1);
}
