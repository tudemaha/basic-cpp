#include <iostream>

using namespace std;

int main() {
    int n;

    cout << "PROGRAM POLA SEGITIGA SIKU-SIKU \n \n";
    cout << "Masukkan jumlah bintang: ";
    cin >> n;

    // Pola 1
    cout << "Pola 1 \n";
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= i; j++) {
            cout << "*";
        }
        cout << "\n";
    }
    cout << "\n";

    // Pola 2
    cout << "Pola 2 \n";
    for(int i = 1; i <= n; i++) {
        for(int j = n; j >= i; j--) {
            cout << "*";
        }
        cout << "\n";
    }
    cout << "\n";

    // Pola 3
    cout << "Pola 3 \n";
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j < i; j++) {
            cout << " ";
        }
        for(int k = n; k >= i; k--) {
            cout << "*";
        }
        cout << "\n";
    }
    cout << "\n";

    // Pola 4
    cout << "Pola 4 \n";
    for(int i = 1; i <= n; i++) {
        for(int j = n; j > i; j--) {
            cout << " ";
        }
        for(int k = 1; k <= i; k++) {
            cout << "*";
        }
        cout << "\n";
    }

    return 0;
}
