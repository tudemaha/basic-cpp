#include <iostream>
#include <limits>

using namespace std;

int main() {
    // Bilangan bulat
    unsigned int a = 5; //unsigned digunakan agar tidak ada tanda negatif
    long b = 5;
    short c = 3;

    // Bilangan desimal
    float d = 3.0;
    double e = 9.4;

    // Character
    char f = 'e';

    // Boolean
    bool g = true;

    cout << a << endl;
    cout << sizeof(a) << " byte" << endl;
    cout << numeric_limits<unsigned int>::max() << endl;
    cout << numeric_limits<unsigned int>::min() << endl;
    return 0;
}
