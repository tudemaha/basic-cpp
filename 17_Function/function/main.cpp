#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int x;

    cout << "Masukkan bilangan untuk diketahui akar kuadratnya: ";
    cin >> x;

    cout << "\n" << "Akar kuadratnya: " << sqrt(x) << endl;
    return 0;
}
