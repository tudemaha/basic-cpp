#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main() {

    while(true) {
        char lempar;

        srand(time(NULL)); // randomize seed

        cout << "Lempar dadu? (y/n): ";
        cin >> lempar;
        if(lempar == 'y') {
            cout << 1 + (rand() % 6) << endl;
        } else if(lempar == 'n') {
            break;
        } else {
            cout << "Perintah yang dimasukkan tidak valid! \n";
        }
    }


    return 0;
}
