#include <iostream>

using namespace std;

void luas(float panjang, float lebar) {
    float luas = panjang * lebar;
    cout << luas;
}

void keliling(float panjang, float lebar) {
    float keliling = 2 * (panjang + lebar);
    cout << keliling;
}

int main() {

    float panjang, lebar;

    cout << "APLIKASI PENGHITUNG LUAS DAN KELILING PERSEGI PANJANG \n";
    cout << "Masukkan panjang: ";
    cin >> panjang;
    cout << "Masukkan lebar: ";
    cin >> lebar;

    cout << "\n";

    cout << "Keliling: ";
    keliling(panjang, lebar);
    cout << "\nLuas: ";
    luas(panjang, lebar);

    return 0;
}
