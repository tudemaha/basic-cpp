#include <iostream>
#include <array>

using namespace std;

int main() {
    // membuat array di standard library
    // array<tipe_data, jumlah_array> nama_array;

    array<int, 5> nilai;

    for(int i = 0; i < 5; i++) {
        nilai[i] = i;
        cout << "nilai [" << i << "] : " << nilai[i] << " address : " << &nilai[i] << endl;
    }
    cout << endl;

    // ukuran array
    cout << "ukuran : " << nilai.size() << endl;

    // address awal array
    cout << "address awal: " << nilai.begin() << endl;

    // address akhir array
    cout << "address akhir: " << nilai.end() << endl;

    // nilai dengan index
    cout << "nilai ke 4: " << nilai.at(4) << endl;
}