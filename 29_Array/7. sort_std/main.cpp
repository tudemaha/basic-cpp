#include <iostream>
#include <array>
#include <algorithm>

const size_t panjang = 10;


// overloading fungsi
void cetak_array(std::array<int, panjang> angka) {
    std::cout << "Array: ";
    for(int cetak : angka) {
        std::cout << cetak << " ";
    }
    std::cout << std::endl;
}

void cetak_array(std::array<char, panjang> huruf) {
    std::cout << "Array: ";
    for(char cetak : huruf) {
        std::cout << cetak << " ";
    }
    std::cout << std::endl;
}

int main() {

    std::array<int, panjang> angka = {4, 5, 3, 0, 2, 7, 1, 9, 8, 6};
    std::array<char, panjang> huruf = {'e', 'c', 'y', 'h', 'l', 'i', 'o', 'z', 'a', 'q'};

    cetak_array(angka);
    cetak_array(huruf);


    // sort isi array (ada di library algorithm)
    std::sort(angka.begin(), angka.end());
    std::sort(huruf.begin(), huruf.end());

    std::cout << std::endl;
    cetak_array(angka);
    cetak_array(huruf);
    

    return 0;
}