#include <iostream>
#include <array>

using namespace std;

int main() {
    array<int, 11> nilai;

    cout << "===== PROGRAM GRAFIK NILAI =====" << endl;
    cout << "================================" << endl << endl;

    // input banyak mahasiswa
    cout << "Banyak mahasiswa mendapatkan nilai: " << endl;
    for(int i = 0; i < nilai.size(); i++) {
        if(i == 10) {
            cout << "100  : ";
        } else {
            cout << (i * 10) << "-" << (i * 10) + 9 << ": ";
        }
        cin >> nilai[i];
    }

    // menampilkan grafik nilai
    cout << endl << "Grafik Nilai" << endl;
    for(int i = 0; i < nilai.size(); i++) {
        if(i == 10) {
            cout << "100  : ";
        } else {
            cout << (i * 10) << "-" << (i * 10) + 9 << ": ";
        }
        for(int bintang = 0; bintang < nilai[i]; bintang++) {
            cout << "*";
        }
        cout << endl;
    }

    return 0;
}