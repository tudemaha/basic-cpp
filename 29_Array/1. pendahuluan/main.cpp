#include <iostream>

using namespace std;

int main() {
    int nilai[6] = {0, 1, 2, 3, 4};

    cout << &nilai[0] << " nilainya: " << nilai[0] << endl;
    cout << &nilai[1] << " nilainya: " << nilai[1] << endl;
    cout << &nilai[2] << " nilainya: " << nilai[2] << endl;
    cout << &nilai[3] << " nilainya: " << nilai[3] << endl;
    cout << &nilai[4] << " nilainya: " << nilai[4] << endl;

    // manipulasi nilai array dengan pointer
    int *ptr = nilai;
    *(ptr + 2) = 6; // mengubah nilai dari array ke 3 (relatif terhadap nilai[0])

    nilai[3] = 7;   // cara mengubah langsung tanpa pointer

    cout << endl;
    cout << &nilai[0] << " nilainya: " << nilai[0] << endl;
    cout << &nilai[1] << " nilainya: " << nilai[1] << endl;
    cout << &nilai[2] << " nilainya: " << nilai[2] << endl;
    cout << &nilai[3] << " nilainya: " << nilai[3] << endl;
    cout << &nilai[4] << " nilainya: " << nilai[4] << endl;

    // melihat jumlah member array
    cout << endl;
    cout << "ukuran array = " << sizeof(nilai) << " byte" << endl;  // mengembalikan byte array
    cout << "jumlah member array = " << sizeof(nilai)/sizeof(int) << endl;  //dibagi dulu dengan size int (sesuai tipe data) agar dapat jml member

    return 0;
}