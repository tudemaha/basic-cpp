#include <iostream>
#include <array>

using namespace std;

const int baris = 2, kolom = 3;

void cetak_array(array< array<int, kolom> , baris> matrix) {
    for(array<int, kolom> vektorBaris : matrix) {   // matriks dipecah dulu jadi 2 sesuai jumlah baris (yg isinya masing2 3)
        cout << "[ ";
        for(int vektorKolom : vektorBaris) {        // matriks dari baris n baru dimasukkan ke variabel vektorKolom
            cout << vektorKolom << " ";
        }
        cout << "]";
        cout << endl;
    }
}


int main() {
    array< array<int, kolom> , baris> matrix = {0, 1, 2, 3, 4, 5};

    cetak_array(matrix);

    return 0;
}