#include <iostream>
#include <array>
#include <algorithm>

const size_t size = 10;

void cetak (std::array<int, size> angka) {
    std::cout << "Array: ";
    for(int cetak : angka) {
        std::cout << cetak << " ";
    }
    std::cout << std::endl;
}

int main() {

    int angkaCari;
    bool match;

    std::array<int, size> angka = {5, 7, 2, 0, 1, 8, 3, 4, 9, 6};
    cetak(angka);

    std::cout << "Cek angka yang ingin diketahui ada/tidak: ";
    std::cin >> angkaCari;

    // sort dulu
    std::sort(angka.begin(), angka.end());

    // baru search
    match = std::binary_search(angka.begin(), angka.end(), angkaCari);
    
    if(match) std::cout << "Ketemu!";   // kondisi jika ketemu
    else std::cout << "Tidak ketemu!";  // kondisi jika tidak ketemu

    return 0;
}