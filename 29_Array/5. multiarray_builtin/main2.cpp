#include <iostream>

using namespace std;

int main() {
    // deklarasi multi dimensi
    int matrix[3][2];

    // deklarasi  + inisialisasi multidimensi
    /*
        int matrix[3][2] = {
            {1, 3},
            {8,10},
            {-1, 9}
        };
    */

    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 2; j++) {
            cout << "Masukkan elemen ke-[" << i << "][" << j << "] = ";
            cin >> matrix[i][j];
        }
    }

    cout << endl << "Tampilan matriks:" << endl;
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 2; j++) {
            cout << matrix[i][j] << "\t";
        }
        cout << endl;
    }
    return 0;
}