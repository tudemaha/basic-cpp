#include <iostream>

using namespace std;

void cetakArray(int *array, int baris, int kolom) {
    int index = 0;
    
    for(int i = 0; i < baris; i++) {
        cout << "[ ";
        for(int j = 0; j < kolom; j++) {
            cout << *(array + index) << " ";
            index++;
        }
        cout << "]";
        cout << endl;
    }
}

int main() {
    int baris = 2, kolom = 2;

    int nilai[baris][kolom] = {1, 2, 3, 4};
    
    cetakArray(*nilai, baris, kolom);

    return 0;
}