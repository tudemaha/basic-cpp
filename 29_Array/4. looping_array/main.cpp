#include <iostream>

using namespace std;

int main() {
    int nilaiArr[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    // looping c++11 ke atas
    /*
        for(deklarasi_variabel : array) {
            statement;
        }
    */

   // menampilkan nilai pada array
   for(int nilai : nilaiArr) {
       cout << "address " << &nilai << " nilainya " << nilai << endl;
   }

   // memanipulasi nilai array
   cout << endl;
   for(int &nilaiRef : nilaiArr) {
       nilaiRef += 2;
       //cout << "address " << &nilaiRef << " nilainya " << nilaiRef << endl;
   }

   // membuktikan nilai awal berubah
   cout << endl;
   for(int nilai : nilaiArr) {
       cout << "address " << &nilai << " nilainya " << nilai << endl;
   }

    return 0;
}