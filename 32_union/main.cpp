#include <iostream>

using namespace std;

// union itu 1 alamat data diisi oleh beberapa tipe data, yang diambil adalah tipe data terpanjang

union nama {
    int int_value;
    char char_value[4];
};

int main() {
    nama beda;

    beda.int_value = 12345;
    
    cout << "data int_value: " << beda.int_value << endl;
    cout << "data char_value: " << beda.char_value << endl << endl;

    beda.char_value[0] = 'a';
    beda.char_value[1] = 'b';
    beda.char_value[2] = 'c';
    beda.char_value[3] = 'd';

    cout << "data int_value: " << beda.int_value << endl;
    cout << "data char_value: " << beda.char_value << endl;

    return 0;
} 