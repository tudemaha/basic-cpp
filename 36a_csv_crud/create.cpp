#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    fstream myFile;

    int jumlah, nim;
    string nama, jurusan;

    myFile.open("report.csv", ios::out | ios::app);

    cout << "Masukkan jumlah mahasiswa: ";
    cin >> jumlah;
    cout << endl;

    while(getchar() != '\n');

    for(int i = 1; i <= jumlah; i++) {
        cout << "Nama Mahasiswa " << i << ": ";
        getline(cin, nama);

        cout << "NIM Mahasiswa " << i << ": ";
        cin >> nim;

        while(getchar() != '\n');

        cout << "Jurusan Mahasiswa " << i << ": ";
        getline(cin, jurusan);

        myFile << nama << ","
               << nim << ","
               << jurusan
               << "\n";
    }

    myFile.close();

    return 0;
}