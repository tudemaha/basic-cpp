#include <iostream>

using namespace std;

// Overloading
//Fungsi utama
int luas(int panjang, int lebar) {
    int luas = panjang * lebar;
    return luas;
}

// Overloading
int luas(int sisi) {
    int luas = sisi * sisi;
    return sisi;
}

double luas(double sisi) {
    return sisi * sisi;
}

/*
    Overloading digunakan untuk menimpa fungsi yang sama yang telah dibuat sebelumnya
    Overloading juga dapat digunakan untuk membuat fungsi dengan tipe data return yang berbeda (dalam konteks nama fungsi sama)
*/

int main() {
    cout << "Luas kotak 2x3: " << luas(2,3) << endl;
    cout << "Luas kotak 3x3: " << luas(3) << endl;
    cout << "Luas kotak 3.5x3.5: " << luas(3.5) << endl;

    return 0;
}
