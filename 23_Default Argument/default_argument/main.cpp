#include <iostream>

using namespace std;

// Default argument harus diletakkan di prototipe fungsi, jangan di fungsi utama
double volume(double p = 1, double l = 1, double t = 1);


int main() {
    cout << "Volume kubus: " << volume(3,4,5) << endl;
    cout << "Volume kubus: " << volume(3,4) << endl;
    cout << "Volume kubus: " << volume(3) << endl;

    return 0;
}

// Default argument pada fungsi
double volume(double p, double l, double t) {
    return p*l*t;
}
