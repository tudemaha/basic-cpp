#include <iostream>

using namespace std;

int main() {
    float a, b, result;
    char symbol;
    string warning;

    cout << "SELAMAT DATANG DI APLIKASI KALKULATOR SEDERHANA \n \n";

    // User input
    cout << "Masukkan angka pertama: ";
    cin >> a;

    cout << "Masukkan opertor aritmatika (+, -, *, /): ";
    cin >> symbol;

    cout << "Masukkan angka kedua: ";
    cin >> b;

    /*
    // if statement
    if(symbol == '+') {
        result = a + b;
    } else if (symbol == '-') {
        result = a - b;
    } else if (symbol == '*') {
        result = a * b;
    } else if (symbol == '/') {
        result = a / b;
    } else {
        warning = "Operator aritmatika yang dimasukkan tidak valid";
    }
    */

    // Switch case statement
    switch(symbol) {
        case '+':
            result = a + b;
            break;
        case '-':
            result = a - b;
            break;
        case '*':
            result = a * b;
            break;
        case '/':
            result = a / b;
            break;
        default:
            warning = "Operator aritmatika yang dimasukkan tidak valid";
    }

    // Result statement
    if(warning == "") {
        cout << "\n" << a << symbol << b << "=" << result << "\n";
    } else {
        cout << "\n" << warning << endl;
    }

}
