#include <iostream>

using namespace std;

int main() {
    int a = 1;
    cout << "Nilai a = " << a << endl;
    cout << "Alamat a = " << &a << endl << endl;

    // reference (untuk menautkan suatu variabel ke variabel yang lain)
    // Jika mengubah satu variabel maka variabel lain juga akan ikut berubah
    int &b = a;
    cout << "Nilai b = " << b << endl;
    cout << "Alamat b = " << &b << endl << endl;

    // Ubah nilaii b
    b = 10;
    cout << "Nilai a = " << a << endl;
    cout << "Nilai b = " << b << endl << endl;

    // Ubah nilai a
    a = 20;
    cout << "Nilai a = " << a << endl;
    cout << "Nilai b = " << b << endl;
    return 0;
}
