#include <iostream>

using namespace std;

// Tutorial WPU
int tampilAngka(int n) {
    // Base case (untuk memberhentikan rekursif)
    if(n == 0) {
        return 0;
    }

    cout << n << endl;
    return tampilAngka(n-1);
}

int faktorial(int f) {
    if(f == 1) return 1;

    return f * faktorial(f-1);
}

// Tutorial Kelas Terbuka (dengan cara WPU)
int pangkat(int a, int b) {
    if(b == 1) {    // Bisa b == 0, tapi yg di-return 1
        cout << "akhir dari rekursif" << endl;
        return a;
    }
    cout << "rekursif" << endl;
    return a * pangkat(a, (b-1));
}


int main() {
    //tampilAngka(10);
    //cout << faktorial(5);

    cout << pangkat(3,6);
    return 0;
}
