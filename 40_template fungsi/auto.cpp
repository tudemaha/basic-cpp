#include <iostream>
#include <string>
#include <typeinfo>

using namespace std;

// gunakan auto agar tipe data yang dikembalikan sama dengan input
// bisa juga jadi tempate<auto T, auto U>
template<typename T, typename U>
auto max(T data1, U data2) {
    return ((data1 > data2) ? data1 : data2);
}

int main() {
    //jangan pakai auto terus
    int a = 10;
    string b = "halo";
    double c = 10.8;
    float d = 87.9f;

    // auto memilih tipe data dengan bit paling panjang
    auto e = max(23, 4);

    
    cout << a << " tipe: " << typeid(a).name() << endl;
    cout << b << " tipe: " << typeid(b).name() << endl;
    cout << c << " tipe: " << typeid(c).name() << endl;
    cout << d << " tipe: " << typeid(d).name() << endl;

    cout << e << " tipe: " << typeid(e).name() << endl;

    return 0;
}