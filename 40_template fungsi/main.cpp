#include <iostream>

using namespace std;

// void print(int data) {
//     cout << data << endl;
// }

template<typename T>
void print(T data) {
    cout << data << endl;
}

// misalkan konversi ke integer
template<typename T>
int toInt(T data) {
    return (int)data;
}

// akan mengembalikan data sesuai dengan tipe data T yang diterima
template<typename T, typename U>
T max(T data1, U data2) {
    return ((data1 > data2) ? data1 : data2);
}

int main() {
    print(toInt('k'));
    print(max('c', 108.7));

    // menentukan tipe data untuk si template
    print<double>(10.4);
    cout << max<int, double>(19, 108.7) << endl;
    return 0;
}