#include <iostream>

using namespace std;

int main() {
    // typedef => memberikan alias untuk tipe data

    typedef int i;
    typedef int iVector[2];
    typedef unsigned long ulong;
    typedef string str;

    using numbers = double; // bisa juga seperti ini

    i a = 10;
    iVector b = {2, 6};
    ulong c = 39434;
    numbers d = 34.23;
    str cek = "aku";
    

    cout << a << endl;
    cout << b[0] << " dan " << b[1] << endl;
    cout << c << endl;
    cout << d << endl;
    cout << cek << endl;
    

    return 0;
}